package com.hcl.stockTrading.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.stockTrading.domain.PlacedOrder;

@Repository
public interface PersistOrder extends JpaRepository<PlacedOrder, Integer> {

}
