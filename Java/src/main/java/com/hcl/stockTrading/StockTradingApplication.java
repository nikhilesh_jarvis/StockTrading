package com.hcl.stockTrading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories("com.hcl.stockTrading.dao")
@ComponentScan (basePackages = {"com.hcl.*"})
@EntityScan (basePackages = {"com.hcl.stockTrading.domain"})
public class StockTradingApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockTradingApplication.class, args);
	}
}
