package com.hcl.stockTrading.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.hcl.stockTrading.dao.GetOrdersDao;
import com.hcl.stockTrading.dao.PersistOrder;
import com.hcl.stockTrading.domain.PlacedOrder;

@Repository
@PropertySource("classpath:application.properties")
public class GetAllOrderImpl implements GetOrdersDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private Environment env;
	
	@Autowired
	private PersistOrder persistOrder;

	@Override
	public List<PlacedOrder> getAllOrder() {

		List<PlacedOrder> tempList = entityManager.createQuery(env.getProperty("getallorders"), PlacedOrder.class)
				.getResultList();

		return tempList;
	}

	@Override
	@Transactional
	public Boolean placeOrder(PlacedOrder placedOrder) {
	
		persistOrder.saveAndFlush(placedOrder);
		return true;
	}
	
	@Override
	public List<PlacedOrder> getAvailableStock() {
		List<PlacedOrder> orderList = entityManager.createQuery(env.getProperty("getallorders"),PlacedOrder.class).getResultList();
		
		return orderList;
	}

}
