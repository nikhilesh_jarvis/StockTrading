package com.hcl.stockTrading.model;

import java.util.ArrayList;
import java.util.List;

import com.hcl.stockTrading.domain.PlacedOrder;

public class AvailableStockList {

	private List<PlacedOrder> availableStockList=new ArrayList<PlacedOrder>();

	public List<PlacedOrder> getAvailableStockList() {
		return availableStockList;
	}

	public void setAvailableStockList(List<PlacedOrder> availableStockList) {
		this.availableStockList = availableStockList;
	}

	

}
