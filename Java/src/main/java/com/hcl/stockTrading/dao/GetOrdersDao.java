package com.hcl.stockTrading.dao;

import java.util.List;

import com.hcl.stockTrading.domain.PlacedOrder;

public interface GetOrdersDao {
	
	public List<PlacedOrder> getAllOrder();

	public Boolean placeOrder(PlacedOrder placedOrder);
	public List<PlacedOrder> getAvailableStock();

}
