package com.hcl.stockTrading.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.stockTrading.dao.GetOrdersDao;
import com.hcl.stockTrading.domain.PlacedOrder;
import com.hcl.stockTrading.service.OrdersService;

@Service
public class OrdersServiceImpl implements OrdersService {

	@Autowired
	private GetOrdersDao getOrdersDao;
	@Override
	public List<PlacedOrder> getAllOrders() {
		
		return getOrdersDao.getAllOrder();
	}
	@Override
	public Boolean placedOrderRequest(PlacedOrder placedOrder) {
		
		return getOrdersDao.placeOrder(placedOrder);
	}
	@Override
	public List<PlacedOrder> getAvailableStock() {
		
		return getOrdersDao.getAvailableStock();
	}

}
