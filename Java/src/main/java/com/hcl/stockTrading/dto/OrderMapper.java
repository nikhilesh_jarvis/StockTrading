package com.hcl.stockTrading.dto;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.hcl.stockTrading.domain.PlacedOrder;

@Component
public class OrderMapper {
	
	public PlacedOrder getPlacedOrder(PlaceOrderDTO placeOrderDTO)
	{
		PlacedOrder placedOrder=new PlacedOrder();
		placedOrder.setCreatedTs(new Date());
		placedOrder.setLastModifiedTs(new Date());
		placedOrder.setPrice(placeOrderDTO.getPrice());
		placedOrder.setQty(placeOrderDTO.getQuantity());
		placedOrder.setStatus("pending");
		placedOrder.setSymbol(placeOrderDTO.getSymbol());
		placedOrder.setTotalAmt(placeOrderDTO.getPrice()*placeOrderDTO.getQuantity());
		placedOrder.setType(placeOrderDTO.getType());
		placedOrder.setUserid(placeOrderDTO.getUserId());
		
		return placedOrder;
	}

}
