package com.hcl.stockTrading.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.stockTrading.domain.PlacedOrder;
import com.hcl.stockTrading.dto.OrderMapper;
import com.hcl.stockTrading.dto.PlaceOrderDTO;
import com.hcl.stockTrading.model.AvailableStockList;
import com.hcl.stockTrading.model.StatusList;
import com.hcl.stockTrading.service.OrdersService;

@RestController
public class StockTradingController {
	
	@Autowired
	private OrdersService ordersService;
	
	@Autowired
	private OrderMapper orderMapper;

	@CrossOrigin
	@GetMapping("order/getStatus")
	public ResponseEntity<StatusList> getOrder()
	{
		List<PlacedOrder> orderList=ordersService.getAllOrders();
		StatusList statusList=new StatusList();
		statusList.setStatusList(orderList);
		return new ResponseEntity<StatusList>(statusList,HttpStatus.OK);
	}
	
	@CrossOrigin
	@PostMapping(value = "/order/placeOrder")
	public ResponseEntity<String> placeOrder(@RequestBody PlaceOrderDTO placeOrderDto){
		PlacedOrder order=orderMapper.getPlacedOrder(placeOrderDto);
		
		Boolean status =ordersService.placedOrderRequest(order);
		HttpHeaders header = new HttpHeaders();
		if(status){
			return new ResponseEntity(header, HttpStatus.CREATED);
		}else{
			return new ResponseEntity(header, HttpStatus.EXPECTATION_FAILED);
		}
	}
	@GetMapping("/order/availableStock")
	public ResponseEntity<AvailableStockList> getAvailableStock(){
		List<PlacedOrder> availableStock = ordersService.getAvailableStock();
		AvailableStockList availableStockList =  new AvailableStockList();
		availableStockList.setAvailableStockList(availableStock);
		return new ResponseEntity<AvailableStockList>(availableStockList,HttpStatus.OK);
	}

}
