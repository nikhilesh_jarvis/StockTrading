package com.hcl.stockTrading.service;

import java.util.List;

import com.hcl.stockTrading.domain.PlacedOrder;

public interface OrdersService {
	
	public List<PlacedOrder> getAllOrders();
	public Boolean placedOrderRequest(PlacedOrder placedOrder);

}
