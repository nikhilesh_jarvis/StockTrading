package com.hcl.stockTrading.model;

import java.util.ArrayList;
import java.util.List;

import com.hcl.stockTrading.domain.PlacedOrder;

public class StatusList {
	
	private List<PlacedOrder> statusList=new ArrayList<PlacedOrder>();

	public List<PlacedOrder> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<PlacedOrder> statusList) {
		this.statusList = statusList;
	}

}
