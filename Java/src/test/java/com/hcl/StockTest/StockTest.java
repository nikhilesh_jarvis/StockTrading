package com.hcl.stockTradiing.Stocktest;

import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import com.hcl.stockTrading.dao.GetOrdersDao;
import com.hcl.stockTrading.domain.PlacedOrder;
import com.hcl.stockTrading.service.OrdersService;
import junit.framework.Assert;

@SuppressWarnings("deprecation")
@RunWith(MockitoJUnitRunner.class)
public class StockTest {

	@Mock
	private GetOrdersDao daoMock;
	@InjectMocks
	private OrdersService service;
	List<PlacedOrder> placeOrder = service.getAllOrders();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testgetAllOrder_returnsTestgetAllOrder() {
		when(service.getAllOrders()).thenReturn(daoMock.getAllOrder());
		Assert.assertNotNull(daoMock.getAllOrder());

	}

}
